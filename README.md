# Quality Controls

## A Library for Calculating UCL and LCL Bounds

This is simple library for calculating quality controls bounds. Currently there are 2 types of formulas.

1. Regular control bounds, with include both population and sample datasets.
2. P-Chart formula

### Regular Control Bounds

Here is an example of control bounds for regular population dataset

```javascript
import { DataWithControls } from "quality_controls/dist/DataWithControls";

const myDefects = [/* numbers go here*/]
const myDatasetWithControls = new DataWithControls(myDefects)
```

```javascript
// or like this:
const myDatasetWithControls = new DataWithControls(myDefects, "population")
// or like this
const myDatasetWithControls = new DataWithControls(myDefects, 0)
```

To change the dataset type from a population to a sample you would set it like this:

```javascript
const mySampleDatasetWithControls = new DataWithControls(myDefects, "sample")
// or like this:
const mySampleDatasetWithControls = new DataWithControls(myDefects, 1)
```

### P-Chart Control Bounds

For application of P-Chart Control Bounds you also need an array of numbers representing the population size

```javascript
import { PercentDataWithControls } from "quality_controls/dist/PercentDataWithControls";

const myDefects = [/* numbers go here*/]
const myPopSizes = [/* numbers go here*/]

const myPChartData = new PercentDataWithControls(myDefects, myPopSizes)
```

### Methods

1. getMeanArr(): Returns an array of number which are the mean.
2. getLowerBoundsArr(): Returns an array of numbers representing the LCL
3. getUpperBoundsArr(): Returns an array of numbers representing the UCL
4. getData(): returns an object with arrays of relevant data:
    * defects: An array the count of defects
    * mean: an array of number which are the mean.
    * upperBounds: an array of numbers representing the UCL
    * lowerBounds: an array of numbers representing the LCL
    * populationSizes: [ONLY for P-Charts] an array of numbers representing the population sizes
    * percent: [ONLY for P-Charts] an array of numbers representing the defects as a percent
