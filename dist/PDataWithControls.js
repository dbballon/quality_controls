"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PercentDataWithControls = void 0;
var utils_1 = require("./utils");
var PercentDataWithControls = /** @class */ (function () {
  function PercentDataWithControls(ticks, sampleSizes) {
    this.ticks = ticks;
    this.sampleSizes = sampleSizes;
    this.p = utils_1.precN(utils_1.sum(ticks) / utils_1.sum(sampleSizes), 10);
  }
  PercentDataWithControls.prototype.calcPControls = function (p) {
    return function (n) {
      return 3 * Math.sqrt((p * (1 - p)) / n);
    };
  };
  PercentDataWithControls.prototype.getMeanArr = function () {
    return utils_1.repeatElement(this.p, this.ticks.length);
  };
  PercentDataWithControls.prototype.getLowerBoundsArr = function () {
    var _this = this;
    var controls = this.calcPControls(this.p);
    return this.sampleSizes.map(function (sampleSize) {
      return utils_1.precN(_this.p - controls(sampleSize), 10) * 100;
    });
  };
  PercentDataWithControls.prototype.getUpperBoundsArr = function () {
    var _this = this;
    var controls = this.calcPControls(this.p);
    return this.sampleSizes.map(function (sampleSize) {
      return utils_1.precN(_this.p + controls(sampleSize), 10) * 100;
    });
  };
  return PercentDataWithControls;
})();
exports.PercentDataWithControls = PercentDataWithControls;
//# sourceMappingURL=PercentDataWithControls.js.map
