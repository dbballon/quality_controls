"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getGraphsData = void 0;
const utils_1 = require("./utils");
const getGraphsData = (d) => ({
    defects: d.defects,
    populationSizes: d.populationSizes ? d.populationSizes : [],
    percent: d.defects.map((defect, i) => d.populationSizes && d.populationSizes[i]
        ? utils_1.precN(defect / d.populationSizes[i], 10) * 100
        : defect),
    mean: d.getMeanArr(),
    upperBounds: d.getUpperBoundsArr(),
    lowerBounds: d.getLowerBoundsArr(),
});
exports.getGraphsData = getGraphsData;
//# sourceMappingURL=getGraphsData.js.map