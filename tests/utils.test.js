/* eslint-disable no-undef */
import { precN, sum } from "../src/utils";

describe("Calculate sum", () => {
  it("Should take an array and return the sum of all elements", () => {
    expect(sum([1, 2, 3, 4, 5])).toEqual(15);
  });
});

describe("precN", () => {
  it("Should take a float and return a float toPrecision of specified number.", () => {
    expect(precN(0.6698362387744321, 10)).toEqual(0.6698362388);
  });
});
