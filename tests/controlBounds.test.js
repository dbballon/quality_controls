/* eslint-disable no-undef */
import { calcControlBounds, calcPControlBounds } from "../src/controlBounds";

describe("Calculate control bounds", () => {
  it("Should return the control bounds for an array", () => {
    const arr1 = [
      7, 10, 6, 10, 4, 7, 5, 11, 4, 11, 8, 11, 10, 4, 8, 6, 11, 10, 6, 11, 4,
    ];
    const [lower1, upper1] = calcControlBounds(arr1);
    expect(lower1).toEqual(-0.13);
    expect(upper1).toEqual(15.75);
  });
});

describe("Calculate control bounds for P Charts", () => {
  it("Should return proper percentage control bounds", () => {
    const defects = [
      58, 60, 68, 62, 60, 72, 58, 64, 66, 56, 64, 68, 62, 70, 64, 58, 68, 64,
      66, 60,
    ];
    const sampleSizes = [
      80, 94, 85, 95, 86, 103, 82, 109, 103, 89, 90, 100, 110, 99, 103, 94, 78,
      110, 100, 80,
    ];

    expect(calcPControlBounds(defects, sampleSizes)).toEqual([
      {
        UCL: 82.85042114,
        CL: 0.6708994709,
        LCL: 51.329473039999996,
      },
      {
        UCL: 81.62947625,
        CL: 0.6708994709,
        LCL: 52.55041793,
      },
      {
        UCL: 82.37985279,
        CL: 0.6708994709,
        LCL: 51.80004139,
      },
      {
        UCL: 81.55274997000001,
        CL: 0.6708994709,
        LCL: 52.62714421,
      },
      {
        UCL: 82.29069806,
        CL: 0.6708994709,
        LCL: 51.88919612,
      },
      {
        UCL: 80.97973637,
        CL: 0.6708994709,
        LCL: 53.20015781000001,
      },
      {
        UCL: 82.65703376,
        CL: 0.6708994709,
        LCL: 51.52286042000001,
      },
      {
        UCL: 80.59203778,
        CL: 0.6708994709,
        LCL: 53.5878564,
      },
      {
        UCL: 80.97973637,
        CL: 0.6708994709,
        LCL: 53.20015781000001,
      },
      {
        UCL: 82.03230953,
        CL: 0.6708994709,
        LCL: 52.14758465,
      },
      {
        UCL: 81.94906452000001,
        CL: 0.6708994709,
        LCL: 52.23082966,
      },
      {
        UCL: 81.18654362,
        CL: 0.6708994709,
        LCL: 52.993350559999996,
      },
      {
        UCL: 80.53052452,
        CL: 0.6708994709,
        LCL: 53.649369660000005,
      },
      {
        UCL: 81.25755967,
        CL: 0.6708994709,
        LCL: 52.92233451,
      },
      {
        UCL: 80.97973637,
        CL: 0.6708994709,
        LCL: 53.20015781000001,
      },
      {
        UCL: 81.62947625,
        CL: 0.6708994709,
        LCL: 52.55041793,
      },
      {
        UCL: 83.0511996,
        CL: 0.6708994709,
        LCL: 51.12869458,
      },
      {
        UCL: 80.53052452,
        CL: 0.6708994709,
        LCL: 53.649369660000005,
      },
      {
        UCL: 81.18654362,
        CL: 0.6708994709,
        LCL: 52.993350559999996,
      },
      {
        UCL: 82.85042114,
        CL: 0.6708994709,
        LCL: 51.329473039999996,
      },
    ]);
  });
});
