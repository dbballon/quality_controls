export function sum(arr: number[]): number {
  return arr.reduce((a, b) => a + b);
}

export function precN(flt: number, prec: number): number {
  return parseFloat(flt.toPrecision(prec));
}

export function repeatElement(el: any, times: number): Array<any> {
  const arr = [];
  for (let i = 0; i < times; i++) {
    arr.push(el);
  }
  return arr;
}
