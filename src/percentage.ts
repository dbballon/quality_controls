export const calcNumerator = (percent: number, denominator: number): number =>
  Math.round((percent * denominator) / 100);

export const calcPercentageDecimal = (
  numerator: number,
  denominator: number
): number => parseFloat((numerator / denominator).toFixed(3));

export const formatPercentage = (dec: number): string =>
  `${(dec * 100).toFixed(3)}%`;
