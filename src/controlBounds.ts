import { calcMean, calcStDevPop } from "./statistics";
import { precN, sum } from "./utils";

type PControlBounds = {
  UCL: number;
  CL: number;
  LCL: number;
};

export const calcControlBounds = (arr: number[]): number[] => {
  const control = calcStDevPop(arr) * 3;
  const mean = calcMean(arr);
  return [
    parseFloat((mean - control).toFixed(2)),
    parseFloat((mean + control).toFixed(2)),
  ];
};

export const makePControlLimitsFunc = (p: number) => (
  n: number
): PControlBounds => {
  const control = 3 * Math.sqrt((p * (1 - p)) / n);
  return {
    UCL: precN(p + control, 10) * 100,
    CL: p,
    LCL: precN(p - control, 10) * 100,
  };
};

export const calcPControlBounds = (
  defects: Array<number>,
  sampleSizes: Array<number>
): PControlBounds[] => {
  const defectSum = sum(defects);
  const sampleSizeSum = sum(sampleSizes);
  const p = precN(defectSum / sampleSizeSum, 10);
  const makePCLs = makePControlLimitsFunc(p);

  return sampleSizes.map((np) => makePCLs(np));
};
